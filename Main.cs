﻿
	
	static void Main( )
	{
		string currentPlayer;
		string location;
		
		Console.ReadLine( );
		Console.WriteLine( );
		Console.WriteLine( "Welcome to BME121 Tic-Tac-Toe" );

		ClearBoard( );
		WriteBoard( );

		xPlayerType = GetPlayerType( "X" );
		oPlayerType = GetPlayerType( "O" );

		currentPlayer = GetFirstPlayer( );

		while( ! IsFullBoard( ) )
		{
			location = GetNextLocation( currentPlayer );

			MarkBoard( currentPlayer, location );
			WriteBoard( );

			if( IsWinForPlayer( currentPlayer ) )
			{
				Console.WriteLine( );
				Console.WriteLine( "Player {0} wins! ({1})", currentPlayer, WinPatterns( currentPlayer ) );
				return;
			}

			currentPlayer = GetNextPlayer( currentPlayer );
		}

		Console.WriteLine( );
		Console.WriteLine( "Game is a draw (no winner)." );
	}