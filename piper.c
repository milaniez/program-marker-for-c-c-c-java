#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>

/* Execute the command using this shell program.  */
#define SHELL "/bin/sh"

#define inpFileAdr	argv[1]
#define outFileAdr	argv[2]
#define solExe 		argv[3]

int main(int argc, char * argv[])
{
	// multiprocess vars
	int status;
	pid_t writerPID, solPID;
	int retStat;
	int PipeSTDIN[2]; // parent > sol (child2)
	int PipeSTDOUT[2]; // sol > child1
	
	// solution vars
	//char solArg0[1024];
	//char solArg1[1024];
	char a[1000];
	FILE* solR;
	FILE* solW;
	//char * solArgs[10];
	
	// writer vars
	unsigned char inpStrW[1024];
	FILE* fileW;
	FILE* inW;
	
	// reader vars (parent)
	unsigned char inpStrR[1024];
	FILE* fileR;
	FILE* outR;
	
	// init vars
	//solArgs[0] = 
    printf("%s\n",argv[1]);
    printf("%s\n",argv[2]);
    printf("%s\n",argv[3]);
    fflush(stdout);

	
	if ((pipe(PipeSTDIN) < 0) || (pipe(PipeSTDOUT))) {
		fprintf(stderr,"Unable to pipe");
		exit(1);
	}
	
	if ((solPID = fork() ) < 0) {
		fprintf(stderr,"Unable to fork sol\n");
		close(PipeSTDIN[0]);
		close(PipeSTDIN[1]);
		close(PipeSTDOUT[1]);
		close(PipeSTDOUT[0]);
		exit(1);
	}
	else if (solPID == 0) { // solution process (child 1)
		if ((writerPID = fork() ) < 0) { // parent process
			fprintf(stderr,"Unable to fork writer\n");
			close(PipeSTDIN[0]);
			close(PipeSTDIN[1]);
			close(PipeSTDOUT[1]);
			close(PipeSTDOUT[0]);
			exit(1);
		} else if (writerPID == 0) { // wrier process (child 2)
			close(PipeSTDIN[0]);
			close(PipeSTDIN[1]);
			close(PipeSTDOUT[1]);
		
			fileW = fopen(outFileAdr,"w");
			inW = fdopen(PipeSTDOUT[0],"r");
			//puts("1\n");
			while(fgets(inpStrW,1023,inW)) {
				fprintf(fileW, "%s", inpStrW);
				printf("%s", inpStrW);
		    	//puts("2\n");
			}
			//puts("3\n");
			fclose(inW);
			fclose(fileW);
		
			close(PipeSTDOUT[0]);
			return EXIT_SUCCESS;
		} else { // sol proc
			close(PipeSTDIN[1]);
		    close(PipeSTDOUT[0]);
		    
		    solR = fdopen(PipeSTDIN[0], "r");
		    solW = fdopen(PipeSTDOUT[1], "w");
		    dup2(fileno(solR),STDIN_FILENO);
		    dup2(fileno(solW),STDOUT_FILENO);
		    fclose(solR);
		    fclose(solW);
		    execvp(solExe,&solExe);
		    
		    /*solR = fdopen(PipeSTDIN[0], "r");
		    solW = fdopen(PipeSTDOUT[1], "w");
		    //puts("4\n");
		    while(fscanf(solR,"%s",a)!=EOF)
		    {
		    	//puts("5\n");
				fprintf(solW,"%s\n",a); fflush(solW);
			}
		   // puts("6\n");*/
		    
			close(PipeSTDIN[0]);
		    close(PipeSTDOUT[1]);
		    fprintf(stderr,"solution exited with error\n");
			//return EXIT_SUCCESS;
			exit(1);
		}
	} else { // parent process
	
		close(PipeSTDOUT[0]);
		close(PipeSTDOUT[1]);
        close(PipeSTDIN[0]);
		
		
		fileR = fopen(inpFileAdr,"w+");
		outR = fdopen(PipeSTDIN[1],"w");
		while(fgets(inpStrR, 1023, stdin)) {
			fputs(inpStrR, outR); fflush(outR);
			fputs(inpStrR, fileR); fflush(fileR);
		}
		//fclose(outR);
		fclose(fileR);
		
		
        close(PipeSTDIN[1]);
        waitpid(solPID, &retStat, 0);
        waitpid(writerPID, &retStat, 0);
		return EXIT_SUCCESS;
	}
}
