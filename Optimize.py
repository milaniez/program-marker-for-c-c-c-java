#!/usr/bin/python3.4

import sys
import re
from FieldManipulator import CurlyLangs

csFileAdr = sys.argv[1]
outputFileAdr = sys.argv[2]
with open(csFileAdr) as csFile:
    csFileContent = csFile.read()

with open('GetRandomLocation.cs') as f:
    getRndLoc = f.read()

with open('Main.cs') as f:
    main = f.read()

replacer = CurlyLangs()
getRanLocPath = "static\s+string\s+GetRandomLocation\s*\(.*?\)"
# print(getRanLocPath)
csFileContent = replacer.Replace("\{","\}",getRanLocPath,csFileContent,getRndLoc)
# mainPath = ["static\s+void\s+Main\s*\(.*?\)"]
# csFileContent = replacer.Replace("\{","\}",mainPath,csFileContent,main)
delayPath = ["static\s+string\s+GetNextLocation\s*\(.*?\)","System\.Threading\.Thread\.Sleep"]
csFileContent = replacer.ReplaceRecursive("\(","\);",delayPath,csFileContent,"")
delayPath = ["static\s+string\s+GetNextLocation\s*\(.*?\)","System\.Threading\.Thread\.Sleep"]
csFileContent = replacer.ReplaceRecursive("\(","\);",delayPath,csFileContent,"")

with open(outputFileAdr,'w') as output:
    output.write(csFileContent)