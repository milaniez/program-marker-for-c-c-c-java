import re
import copy


class CurlyLangs:
    """
    This class is for C/C++, C#, and Java
    """

    def __init__(self, outFields=None):
        if not outFields:
            outFields = [['(")' , '(")'  , ['(\")']], ["(')"  , "(')"  , ["(\')"]],
                         ["(//)", "(\n)",  [      ]], ["(/\*)", "(\*/)", [      ]]]
        self.outField = outFields
        self.outFieldCnt = len(outFields)
        self.outFieldsBeg = []
        self.outFieldsEnd = []
        self.outFieldsNotEnd = []
        for outField in outFields:
            self.outFieldsBeg.append(outField[0])
            self.outFieldsEnd.append(outField[1])
            self.outFieldsNotEnd.append(outField[2])
        self.outFieldBegs = "|".join(self.outFieldsBeg)

    def Find(self, left, right, fieldName, code):
        rightOrig = right
        right = right.strip(";").strip(" \t\n\r\f\v")
        b4Field = ""
        field = ""
        afterField = code
        pattern = "(^.*?)(\s*)(" + self.outFieldBegs + "|(" + fieldName + "))(.*$)"
        prog = re.compile(pattern, re.DOTALL | re.MULTILINE)
        while True:
            matches = prog.match(afterField)
            if not matches:
                print("no matches 1")
                return False, "", "", ""
            if matches.group(self.outFieldCnt + 4):
                b4Field += matches.group(1)
                field = (matches.group(2) + matches.group(3))
                afterField = matches.group(self.outFieldCnt + 5)
                break
            else:
                b4Field += (matches.group(1) + matches.group(2) + matches.group(3))
                afterField = matches.group(self.outFieldCnt + 5)
                outFieldIdx = self.outFieldsBeg.index("(" + matches.group(3) + ")")
                fieldNotEnds = copy.deepcopy(self.outFieldsNotEnd)[outFieldIdx]
                fieldEnd = copy.deepcopy(self.outFieldsEnd)[outFieldIdx]
                matchFields = fieldNotEnds
                matchFields.append(fieldEnd)
                matchFieldsLength = len(matchFields)
                matchFields = "|".join(matchFields)
                pattern2 = "(^.*?)(" + matchFields + ")(.*$)"
                prog2 = re.compile(pattern2, re.DOTALL | re.MULTILINE)
                while True:
                    matches = prog2.match(afterField)
                    if not matches:
                        print("no matches 2")
                        return False, "", "", ""
                    b4Field += (matches.group(1) + matches.group(2))
                    afterField = matches.group(matchFieldsLength + 3)
                    if matches.group(2) == fieldEnd.strip("()"):
                        break

        pattern = "(^\s*?(" + left + "))(.*$)"
        prog = re.compile(pattern, re.DOTALL | re.MULTILINE)
        matches = prog.match(afterField)
        if not matches:
            print("no matches 2.5")
            return False, "", "", ""
        field += matches.group(1)
        afterField = matches.group(3)

        depth = 1
        while depth > 0:
            pattern = "(^.*?)(\s*)(" + self.outFieldBegs + "|(" + left + ")|(" + right + "))(.*$)"
            prog = re.compile(pattern, re.DOTALL | re.MULTILINE)
            matches = prog.match(afterField)
            if not matches:
                print("no matches 3")
                return False, "", "", ""
            field += (matches.group(1) + matches.group(2) + matches.group(3))
            afterField = matches.group(self.outFieldCnt + 6)
            if matches.group(self.outFieldCnt + 4):
                depth += 1
            elif matches.group(self.outFieldCnt + 5):
                depth -= 1
            else:
                outFieldIdx = self.outFieldsBeg.index("(" + matches.group(3) + ")")
                fieldNotEnds = copy.deepcopy(self.outFieldsNotEnd)[outFieldIdx]
                fieldEnd = copy.deepcopy(self.outFieldsEnd)[outFieldIdx]
                matchFields = fieldNotEnds
                matchFields.append(fieldEnd)
                matchFieldsLength = len(matchFields)
                matchFields = "|".join(matchFields)
                pattern2 = "(^.*?)(" + matchFields + ")(.*$)"
                prog2 = re.compile(pattern2, re.DOTALL | re.MULTILINE)
                while True:
                    matches = prog2.match(afterField)
                    if not matches:
                        print("no matches 4")
                        return False, "", "", ""
                    field += (matches.group(1) + matches.group(2))
                    afterField = matches.group(matchFieldsLength + 3)
                    if matches.group(2) == fieldEnd.strip("()"):
                        break

        # check for semicolon
        if right != rightOrig:
            pattern = "(^\s*)(;)(.*$)"
            prog = re.compile(pattern, re.MULTILINE | re.DOTALL)
            matches = prog.match(afterField)
            if matches:
                afterField = matches.group(3)
            else:
                print("no matches 5")
                return False, "", "", ""

        return True, b4Field, field, afterField

    def FindRecursive(self, left, right, fieldNames, code):
        fieldCnt = len(fieldNames)
        b4Field = ""
        field = code
        afterField = ""
        for fieldNo in range(fieldCnt):
            if fieldNo < fieldCnt - 1:
                found, bf, field, af = self.Find("\{", "\}", fieldNames[fieldNo], field)
                if not found:
                    print("field name not found 1")
                    return False, "", "", ""
                b4Field = b4Field + bf
                afterField = af + afterField
            else:
                found, bf, field, af = self.Find(left, right, fieldNames[fieldNo], field)
                if not found:
                    print("field name not found 2")
                    return False, "", "", ""
                b4Field = b4Field + bf
                afterField = af + afterField
        return True, b4Field, field, afterField

    def Get(self, left, right, fieldName, code):
        found, b4Field, field, afterField = self.Find(left, right, fieldName, code)
        if found:
            return field
        else:
            return ""

    def GetRecursive(self, left, right, fieldNames, code):
        found, b4Field, field, afterField = self.FindRecursive(left, right, fieldNames, code)
        if found:
            return field
        else:
            return ""

    def Replace(self, left, right, fieldName, code, block):
        found, b4Field, field, afterField = self.Find(left, right, fieldName, code)
        if found:
            return b4Field + block + afterField
        else:
            return ""

    def ReplaceRecursive(self, left, right, fieldNames, code, block):
        found, b4Field, field, afterField = self.FindRecursive(left, right, fieldNames, code)
        if found:
            return b4Field + block + afterField
        else:
            return ""

    def ReplaceFrom(self, left, right, fieldName, codeSource, codeDest):
        fieldSource = self.Get(left, right, fieldName, codeSource)
        if len(fieldSource) > 0:
            return self.Replace(left, right, fieldName, codeDest, fieldSource)
        else:
            return ""

    def ReplaceFromRecursive(self, left, right, fieldNames, codeSource, codeDest):
        fieldSource = self.GetRecursive(left, right, fieldNames, codeSource)
        if len(fieldSource) > 0:
            return self.ReplaceRecursive(left, right, fieldNames, codeDest, fieldSource)
        else:
            return ""

    def ReplaceAllRecursive(self, left, right, fieldNames, code, block):
        fieldName = fieldNames[-1]
        del fieldNames[-1]
        found, b4Field, field, afterField = self.FindRecursive("\{", "\}", fieldNames, code)
        if not found:
            return ""
        found, b4tmp, tmp, afterTemp = self.Find(left, right, fieldName, field)
        while(found):
            b4Field += (b4tmp + block)
            field = afterTemp
            found, b4tmp, tmp, afterTemp = self.Find(left, right, fieldName, field)
        return b4Field + field + afterField

    # def FindFirsSubField(self, code):
    #     found, b4Field, field, afterField = self.Find("\{", "\}", "", code)
    #     return found, b4Field, field, afterField
    #
    # def FindAllSubFields(self, code):
    #     fieldsNames = []
    #     fields = []
    #     fieldsGaps = []
    #
    #     found, pt1, pt2, pt3 = self.FindFirsSubField(code)
    #     if not found:
    #         return found, fieldsNames, fields, fieldsGaps
    #
    #     # This pattern read reversed strings
    #     fieldNameFinderPat = "^(\s*?)((\))|(\\n[^\n]*?//)|(/*)|([a-zA-Z0-9\s.:<>]*?))"
    #     fieldNameFinderProg =
    #     while found:
    #
    #
    #         found, pt1, pt2, pt3 = self.FindFirsSubField(code)
