#!/usr/bin/python3.4

# Create Correct Output
# 1 - Optimize the code using the OptimizeScript Scrips
# 2 - Compile the Solution code here
# 3 - Use the different input scenarios, create corresponding correct output scenarios

# Create Code Template Reference
# 1 - format the text of the Solution Code and Code Template appropriately
# 2 - compare Solution Code and Code Template and extract Code Template Reference

# mark a specific code
# 1 - format the text of the code appropriately
# 2 - compile the formatted code
# 3 - get the results from running of the code
# 4 - Compare the results, if the results match up mark that run as successfull
# 5 - if not, find the method that causes problems

# Report Fields:
# Head
# File Extension
# report from the original file and formatted file
# number of tab characters in the original file

from lxml import etree
import lxml
import sys
import os
import shutil
from subprocess import PIPE, Popen
import uuid
import shlex
import glob
import FieldManipulator
import csv
import copy


print("By Mehdi Zeinali")
print("usage: Solver <Path-To-Config-File>")

# Read the xml file
configFileAdr = sys.argv[1]
# with open(configFileAdr,"r") as configFile:
#     configStr = configFile.read().encode('ascii')

configTree = etree.parse(configFileAdr)
configRoot = configTree.getroot()

codeTemplate = ""
solutionPath = ""
submissionDir = ""
workingDir = ""
outputDir = ""
optimizerPath = ""
codeStyler = ""
testCaseDir = ""
compiler = ""
testCaseCnt = ""
newTestCases = ""
testCaseGen = ""
rubricFile = ""
stdoutFieldsPath = ""

# get Config
print("Reading configurations ...")
childCnt = len(configRoot)
assignedVals = [False]*14
for childIdx in range(0, childCnt):
    if type(configRoot[childIdx]) != lxml.etree._Element:
        continue
    if configRoot[childIdx].tag.lower() == "CodeTemplate".lower():
        codeTemplate = configRoot[childIdx].text
        assignedVals[0] = True
    elif configRoot[childIdx].tag.lower() == "Solution".lower():
        solutionPath = configRoot[childIdx].text
        assignedVals[1] = True
    elif configRoot[childIdx].tag.lower() == "SubmissionDir".lower():
        submissionDir = configRoot[childIdx].text
        assignedVals[2] = True
    elif configRoot[childIdx].tag.lower() == "WorkingDir".lower():
        workingDir = configRoot[childIdx].text
        assignedVals[3] = True
    elif configRoot[childIdx].tag.lower() == "OutputDir".lower():
        outputDir = configRoot[childIdx].text
        assignedVals[4] = True
    elif configRoot[childIdx].tag.lower() == "Optimizer".lower():
        optimizerPath = configRoot[childIdx].text
        assignedVals[5] = True
    elif configRoot[childIdx].tag.lower() == "CodeStyler".lower():
        codeStyler = configRoot[childIdx].text
        assignedVals[6] = True
    elif configRoot[childIdx].tag.lower() == "TestCaseDir".lower():
        testCaseDir = configRoot[childIdx].text
        assignedVals[7] = True
    elif configRoot[childIdx].tag.lower() == "Compiler".lower():
        compiler = configRoot[childIdx].text
        assignedVals[8] = True
    elif configRoot[childIdx].tag.lower() == "TestCaseCnt".lower():
        testCaseCnt = configRoot[childIdx].text
        assignedVals[9] = True
    elif configRoot[childIdx].tag.lower() == "NewTestCases".lower():
        newTestCases = configRoot[childIdx].text
        assignedVals[10] = True
    elif configRoot[childIdx].tag.lower() == "TestCaseGen".lower():
        testCaseGen = configRoot[childIdx].text
        assignedVals[11] = True
    elif configRoot[childIdx].tag.lower() == "rubric".lower():
        rubricFile = configRoot[childIdx].text
        assignedVals[12] = True
    elif configRoot[childIdx].tag.lower() == "StdoutFields".lower():
        stdoutFieldsPath = configRoot[childIdx].text
        assignedVals[13] = True

if assignedVals.count(False) > 0:
    raise NameError("Not every configuration is assigned")

# 1. check initialization implications
print("Checking configurations ...")
if not os.path.isfile(codeTemplate):
    raise NameError("{0} is not a valid codeTemplate".format(codeTemplate))
if not os.path.isfile(solutionPath):
    raise NameError("{0} is not a valid solution".format(solutionPath))
if not os.path.isdir(submissionDir):
    raise NameError("{0} is not a valid submissionDir".format(submissionDir))
if os.path.isdir(workingDir):
    shutil.rmtree(workingDir)
os.mkdir(workingDir)
if os.path.isdir(outputDir):
    shutil.rmtree(outputDir)
os.mkdir(outputDir)
if not os.path.isdir(testCaseDir):
    raise NameError("{0} is not a valid testCaseDir".format(testCaseDir))
if (newTestCases.lower() != "true") and (newTestCases.lower() != "false"):
    raise NameError("NewTestCases should be either true or false")
if not os.path.isfile(rubricFile):
    raise NameError("{0} is not a valid rubric".format(rubricFile))


replacer = FieldManipulator.CurlyLangs()

# 2. Create Correct Outputs
print("Creating correct output code ...")
# 2.1 Optimize the code
print("    Optimizing solution ...")
solName = os.path.join(workingDir, str(uuid.uuid4()))  # SOL file NAMES
optSolPath = solName + ".cs"  # OPtimized SOLution CODE
if len(optimizerPath.strip()) > 0:
    optArgs = shlex.split(optimizerPath.format(solutionPath, optSolPath))
    optH = Popen(optArgs, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
    try:
        output, error = optH.communicate(timeout=10)
        output = output.decode("utf-8")
        error = error.decode("utf-8")
    except TimeoutError:
        raise NameError("was not able to complete code solution code optimization")
    if len(error.strip()) > 0:
        print("Warning: the following error was produced during code optimization:")
        print(error)
else:
    shutil.copy(solutionPath, optSolPath)

# 2.2 Compile the solution
print ("    Compiling optimized solution ...")
solExePath = solName + ".exe"  # SOLution EXEcutable
compArg = shlex.split(compiler.format(optSolPath, solExePath))
compH = Popen(compArg, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
try:
    output, error = compH.communicate(timeout=10)
    output = output.decode("utf-8")
    error = error.decode("utf-8")
except TimeoutError:
    raise NameError("was not able to complete code solution code optimization")
if output.lower().find("error") != -1:
    raise NameError("Seriously? the solution has errors??!!! SHAME! ;)")

testCaseCnt = int(testCaseCnt)
# 2.3 Create outputs if necessary
if newTestCases.lower() == "true":
    print("    Creating new test cases ...")
    if os.path.isdir(testCaseDir):
        shutil.rmtree(testCaseDir)
    os.mkdir(testCaseDir)
    for testCaseNo in range(testCaseCnt):
        testCaseSubDir = ("Case{0:0" + str(len(str(testCaseCnt))) + "d}").format(testCaseNo)
        os.mkdir(os.path.join(testCaseDir, testCaseSubDir))
        stdinFileAdr = os.path.join(testCaseDir, testCaseSubDir, "in.txt")
        stdoutFileAdr = os.path.join(testCaseDir, testCaseSubDir, "out.txt")
        while True:
            tmp = "Test Case {0}".format(testCaseNo + 1)
            spaceCnt = 79 - len(tmp)
            spaceCntb4 = int(spaceCnt/2)
            spaceCntAfter = spaceCnt - spaceCntb4
            tmp = " "*spaceCntb4 + tmp + " "*spaceCntAfter
            print("\033[1;41m" + tmp + "\033[1;m")
            testerArgs = shlex.split(testCaseGen.format(solExePath, stdinFileAdr, stdoutFileAdr))
            print(testCaseGen.format(solExePath, stdinFileAdr, stdoutFileAdr))
            testerH = Popen(testerArgs, bufsize=1)
            testerH.wait()
            print("\033[1;41m" + " "*31 + "Accept Testcase?" + " "*32 + "\033[1;m")
            prompt = ""
            while (prompt.lower() != 'y') and (prompt.lower() != 'n')\
                    and (prompt.lower() != 'yes') and (prompt.lower() != 'no'):
                prompt = input()
            if (prompt.lower() == 'y') or (prompt.lower() == 'yes'):
                break


# 3 Mark codes
print("Marking codes ...")
# 3.1 Retrive a list of submitted codes
csFiles = glob.glob(submissionDir + "/*.cs")
# 3.2 Retrive rubric
with open(rubricFile) as f:
    rubric = [x.split(" ") for x in filter(None, f.read().strip().split("\n"))]
# 3.2.5 Recreate the solution without Writelines
print("    Removing Stdout writers from solution ...")
with open(optSolPath) as f:
    optSolCode = f.read()
with open(stdoutFieldsPath) as f:
    stdoutFields  = f.read().split("\n")
    del stdoutFields[-1]
for fieldPathNo in range(len(rubric)):
    for stdoutField in stdoutFields:
        fieldPath = copy.deepcopy(rubric[fieldPathNo])
        fieldPath.append(stdoutField)
        optSolCode = replacer.ReplaceAllRecursive("\(","\);",fieldPath,optSolCode,"")
        if not optSolCode.strip(" \t\n\r\f\v"):
            raise NameError("could not find {0} in {1}".
                            format(stdoutField, fieldPathNo))
with open(optSolPath,"w") as f:
    f.write(optSolCode)

# 3.2.6 Compile the solution
print("    Compiling stdout-removed solution ...")
compArg = shlex.split(compiler.format(optSolPath, solExePath))
compH = Popen(compArg, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
try:
    output, error = compH.communicate(timeout=10)
    output = output.decode("utf-8")
    error = error.decode("utf-8")
except TimeoutError:
    raise NameError("was not able to complete code solution code optimization")
if output.lower().find("error") != -1:
    print(output)
    raise NameError("Error in compiling the stdout removed version")

# 3.2.7 Recreate the stdout
print("    Recreating stdout from removed-stdout version of the solution ...")
for testCaseNo in range(testCaseCnt):
    testCaseSubDir = ("Case{0:0" + str(len(str(testCaseCnt))) + "d}").format(testCaseNo)
    stdinFileAdr = os.path.join(testCaseDir, testCaseSubDir, "in.txt")
    with open(stdinFileAdr) as f:
        stdinText = f.read()
    stdoutFileAdr = os.path.join(testCaseDir, testCaseSubDir, "out.txt")
    solArgs = shlex.split(solExePath)
    solExeH = Popen(solArgs, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
    try:
        stdoutText, stderrText = solExeH.communicate(input=stdinText.encode(),timeout=10)
    except TimeoutError:
        raise NameError("Was not able to complete the solution code in time")
    if stderrText:
        print(stderrText.decode())
        raise NameError("It was the error occurred duting the execution of solution code")
    with open(stdoutFileAdr,"w") as f:
        f.write(stdoutText.decode())

# 3.3 Mark every code in the directory
for csFilePath in csFiles:
    studentId = csFilePath.split('/')[-1].strip().split(' ')[0]
    print("    Marking {0} ...".format(studentId))
    codeName = os.path.join(workingDir, str(uuid.uuid4()))  # SOL file NAMES
    reportFileHandle = open(os.path.join(outputDir, studentId + ".csv"), "w", newline="")
    os.mkdir(os.path.join(outputDir, studentId))
    reportWriter = csv.writer(reportFileHandle)
    reportHeader = ["Compiled"]
    for testCaseNo in range(testCaseCnt):
        reportHeader.append("Test Case {0}".format(testCaseNo + 1))
    reportWriter.writerow(reportHeader)
    # 3.3.1 Optimize the code
    print("        optimizing code ...")
    optCodePath = codeName + ".cs"  # OPtimized  CODE
    if len(optimizerPath.strip()) > 0:
        optArgs = shlex.split(optimizerPath.format(csFilePath, optCodePath))
        optH = Popen(optArgs, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
        try:
            output, error = optH.communicate(timeout=10)
            output = output.decode("utf-8")
            # print(output)
            error = error.decode("utf-8")
        except TimeoutError:
            raise NameError("was not able to complete code solution code optimization")
        if len(error.strip()) > 0:
            print("Warning: the following error was produced during code optimization:")
            print(error)
    else:
        shutil.copy(csFilePath, optCodePath)
    print("        removing stdout writes ...")
    with open(optCodePath) as f:
        optCode = f.read()
    with open(stdoutFieldsPath) as f:
        stdoutFields  = f.read().split("\n")
        del stdoutFields[-1]
    for fieldPathNo in range(len(rubric)):
        for stdoutField in stdoutFields:
            fieldPath = copy.deepcopy(rubric[fieldPathNo])
            fieldPath.append(stdoutField)
            optCode = replacer.ReplaceAllRecursive("\(","\);",fieldPath,optCode,"")
            if not optCode.strip(" \t\n\r\f\v"):
                raise NameError("could not find {0} in {1}".
                                format(stdoutField, fieldPathNo))
    with open(optCodePath,"w") as f:
        f.write(optCode)
    replacedOptCodeName = os.path.join(workingDir, str(uuid.uuid4()))
    replacedOptCodePath = codeName + ".cs"
    replacedOptCodeExePath = codeName + ".exe"  # CODE EXEcutable
    compArg = shlex.split(compiler.format(replacedOptCodePath,
                                          replacedOptCodeExePath))
    runnerArg = shlex.split(replacedOptCodeExePath)

    for fieldPathNo in range(len(rubric)):
        print(" "*12 + "compiling for field no {0}".format(fieldPathNo))
        # 3.3.2 Replace fieldPath content
        fieldPath = rubric[fieldPathNo]
        reportLine = []
        # print(fieldPathNo)
        # print(fieldPath[0])
        # print(optSolCodePath)
        # print(optCodePath)
        replacedOptCode = replacer.ReplaceFromRecursive("\{", "\}", fieldPath, optCode, optSolCode)

        # print(fieldPath)
        with open(replacedOptCodePath, "w") as replacedCodeFileHandle:
            replacedCodeFileHandle.write(replacedOptCode)
        # 3.3.3 Compile the code
        compH = Popen(compArg, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
        try:
            output, error = compH.communicate(timeout=10)
            output = output.decode("utf-8")
            error = error.decode("utf-8")
        except TimeoutError:
            raise NameError("was not able to finish compiling in time")
        if output.lower().find("error") != -1:
            reportLine.append("No")
            for i in range(testCaseCnt):
                reportLine.append(",")
            print(error)
        else:
            reportLine.append("Yes")
            for testCaseNo in range(testCaseCnt):
                testCaseSubDir = ("Case{0:0" + str(len(str(testCaseCnt))) + "d}").format(testCaseNo)
                with open(os.path.join(testCaseDir,testCaseSubDir,"in.txt")) as stdinFile:
                    stdinText = stdinFile.read()
                with open(os.path.join(testCaseDir,testCaseSubDir,"out.txt")) as stdoutFile:
                    stdoutText = stdoutFile.read()
                codeExeH = Popen(runnerArg, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=1)
                try:
                    codeStdoutText, codeStderrText = codeExeH.communicate(input=stdinText.encode(),
                                                                          timeout=10)
                    codeStdoutText = codeStdoutText.decode()
                    codeStderrText = codeStderrText.decode()
                except TimeoutError:
                    reportLine.append("Not completed")

                else:
                    if "".join(codeStdoutText.split()) == "".join(stdoutText.split()):
                        reportField = "Successful"
                    else:
                        reportField = "Unsuccessful"
                        with open(os.path.join(outputDir, studentId,"{0}-{1}.txt".
                                format(fieldPathNo, testCaseNo)),"w") as f:
                            f.write(codeStdoutText)
                    reportLine.append(reportField)
        reportWriter.writerow(reportLine)
    reportFileHandle.close()